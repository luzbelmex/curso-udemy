<?php include_once 'includes/templates/header.php' ?>

  <section class="seccion contenedor">
    <h2>La Mejor Conferencia de Diseño Web en Español</h2>
    <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
</section><!--Seccion-->

<section class="seccion contenedor">
  <h2>Calendario de Eventos</h2>

  <?php

  try {
    require_once('includes/funciones/db_conexion.php');
    $sql = " SELECT evento_id, nombre_evento, fecha_evento, hora_evento, cat_evento, icono, nombre_invitado, apellido_invitado ";
    $sql .= " FROM eventos ";
    $sql .= " INNER JOIN categoria_evento ";
    $sql .= " ON eventos.id_cat_evento = categoria_evento.id_categoria ";
    $sql .= " INNER JOIN invitados ";
    $sql .= " ON eventos.id_inv = invitados.invitado_id ";
    $sql .= "ORDER BY evento_id";
    $resultado = $conn->query($sql);
  } catch (\Exception $e) {
    echo $e->getMessage();
  }


  ?>

  <div class="calendario">

    <?php

    $calendario = array();

    while($eventos = $resultado->fetch_assoc() ) {

      // obtener la fecha del evento

      $fecha = $eventos['fecha_evento'];

      $evento = array (
        'titulo' => $eventos['nombre_evento'],
        'fecha' => $eventos['fecha_evento'],
        'hora' => $eventos['hora_evento'],
        'categoria' => $eventos['cat_evento'],
        'icono' => "fa" . " " . $eventos['icono'],
        'invitado' => $eventos['nombre_invitado'] . " " . $eventos['apellido_invitado']
      );

      $calendario [$fecha][] = $evento;

      ?>

<?php    } //while   ?>

<?php

// imprimir todos los Eventos

foreach ($calendario as $dia => $lista_eventos) { ?>

  <h3>
    <i class="fa fa-calendar" ></i>
    <?php

    //linux y mac

    setlocale(LC_TIME, 'es_ES.Utf-8');

    // windows

    setlocale(LC_TIME, 'spanish');

    echo strftime("%A, %d de %B del %Y", strtotime($dia)); ?>
  </h3>

  <?php

  foreach ($lista_eventos as $evento) { ?>
    <div class="dia">

      <p class="titulo"> <?php echo $evento['titulo'] ?> </p>
      <p class="hora" > <i class="far fa-clock" aria-hidden="true" ></i> <?php echo $evento['fecha'] . " " . $evento['hora'] ?> </p>
      <p> <i class=" <?php echo $evento['icono'] ?> " aria-hidden="true"></i> <?php echo $evento['categoria'] ?> </p>
      <p><i class="fas fa-user" aria-hidden="true"></i> <?php echo $evento['invitado'] ?> </p>

    </div>
  <?php } // foreachs de eventos ?>

<?php } // foreach de dias ?>

    <?php

    $conn->close();

     ?>

  </div>

</section>

  <?php include_once 'includes/templates/footer.php' ?>
