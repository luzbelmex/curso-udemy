<?php include_once 'includes/templates/header.php' ?>

  <section class="seccion contenedor">
    <h2>La Mejor Conferencia de Diseño Web en Español</h2>
    <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
</section><!--Seccion-->

<section class="seccion contenedor">
  <h2>Galeria de Fotos</h2>
  <div class="galeria">
    <a href="img/galeria/01.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/01.jpg">
    </a>
    <a href="img/galeria/02.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/02.jpg">
    </a>
    <a href="img/galeria/03.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/03.jpg">
    </a>
    <a href="img/galeria/04.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/04.jpg">
    </a>
    <a href="img/galeria/05.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/05.jpg">
    </a>
    <a href="img/galeria/06.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/06.jpg">
    </a>
    <a href="img/galeria/07.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/07.jpg">
    </a>
    <a href="img/galeria/08.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/08.jpg">
    </a>
    <a href="img/galeria/09.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/09.jpg">
    </a>
    <a href="img/galeria/10.jpg" data-lightbox="galeria">
      <img src="img/galeria/thumbs/10.jpg">
    </a>
  </div>

</section>

  <?php include_once 'includes/templates/footer.php' ?>
