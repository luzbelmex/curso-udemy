<?php include_once 'includes/templates/header.php' ?>

  <section class="seccion contenedor">
    <h2>La Mejor Conferencia de Diseño Web en Español</h2>
    <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
</section><!--Seccion-->
  <section class="programa">
    <div class="contenedor-video">
      <video autoplay loop poster="img/bg-talleres.jpg">
        <source src="video/video.mp4" type="video/mp4">
        <source src="video/video.mp4" type="video/webm">
          <source src="video/video.mp4" type="video/ogv">
      </video>
    </div><!--Contenedor de Video-->
    <div class="contenido-programa">
      <div class="contenedor">
        <div class="programa-evento">
          <h2>Programa del Evento</h2>
          <nav class="menu-programa">
            <a href="#talleres"><i class="fas fa-code" aria-hidden="true"></i>Talleres</a>
            <a href="#conferencias"><i class="fas fa-comment" aria-hidden="true"></i>Conferencias</a>
            <a href="#seminarios"><i class="fas fa-university" aria-hidden="true"></i>Seminarios</a>
          </nav>
          <div id="talleres" class="info-curso ocultar clearfix">
            <div class="detalle-evento">
              <h3>HTML5, CSS3 y JavaScript</h3>
              <p><i class="far fa-clock" aria-hidden="true"></i>16:00 Hrs</p>
              <p><i class="fas fa-calendar" aria-hidden="true"></i>10 de Diciembre</p>
              <p><i class="fas fa-user" aria-hidden="true"></i>Samuel Arrieta Tavares</p>
            </div>
            <div class="detalle-evento">
              <h3>Responsive Web Design</h3>
              <p><i class="far fa-clock" aria-hidden="true"></i>20:00 Hrs</p>
              <p><i class="fas fa-calendar" aria-hidden="true"></i>10 de Diciembre</p>
              <p><i class="fas fa-user" aria-hidden="true"></i>Samuel Arrieta Tavares</p>
            </div>
            <a href="#" class="boton float-right">Ver Todos</a>
          </div><!--Talleres-->
          <div id="conferencias" class="info-curso ocultar clearfix">
            <div class="detalle-evento">
              <h3>Como ser Freelancer</h3>
              <p><i class="far fa-clock" aria-hidden="true"></i>10:00 Hrs</p>
              <p><i class="fas fa-calendar" aria-hidden="true"></i>10 de Diciembre</p>
              <p><i class="fas fa-user" aria-hidden="true"></i>Samuel Arrieta Tavares</p>
            </div>
            <div class="detalle-evento">
              <h3>Tecnologias del Futuro</h3>
              <p><i class="far fa-clock" aria-hidden="true"></i>17:00 Hrs</p>
              <p><i class="fas fa-calendar" aria-hidden="true"></i>10 de Diciembre</p>
              <p><i class="fas fa-user" aria-hidden="true"></i>Samuel Arrieta Tavares</p>
            </div>
            <a href="#" class="boton float-right">Ver Todos</a>
          </div><!--Talleres-->
          <div id="seminarios" class="info-curso ocultar clearfix">
            <div class="detalle-evento">
              <h3>Diseño UI/UX para moviles</h3>
              <p><i class="far fa-clock" aria-hidden="true"></i>17:00 Hrs</p>
              <p><i class="fas fa-calendar" aria-hidden="true"></i>11 de Diciembre</p>
              <p><i class="fas fa-user" aria-hidden="true"></i>Samuel Arrieta Tavares</p>
            </div>
            <div class="detalle-evento">
              <h3>Aprende a programar en una mañana</h3>
              <p><i class="far fa-clock" aria-hidden="true"></i>10:00 Hrs</p>
              <p><i class="fas fa-calendar" aria-hidden="true"></i>11 de Diciembre</p>
              <p><i class="fas fa-user" aria-hidden="true"></i>Samuel Arrieta Tavares</p>
            </div>
            <a href="#" class="boton float-right">Ver Todos</a>
          </div><!--Talleres-->
        </div><!--Progama del evento-->
      </div><!--Contenedor-->
    </div><!--Contenido del Programa-->
  </section><!--Programa-->

  <?php include_once 'includes/templates/invitados.php' ?>

    <div class="contador parallax">
      <div class="contador">
        <ul class="resumen-evento clearfix">
          <li><p class="numero">0</p>Invitados</li>
          <li><p class="numero">0</p>Talleres</li>
          <li><p class="numero">0</p>Dias</li>
          <li><p class="numero">0</p>Conferencias</li>


        </ul>

      </div>

    </div>

  <section class="precios seccion">
    <h2>Precios</h2>
    <div class="contenedor">
      <ul class="lista-precios clearfix">
        <li>
          <div class="tabla-precio">
            <h3>Pase por Dia</h3>
            <p class="numero">$30</p>
            <ul>
              <li>Bocadillos Gratis</li>
              <li>Todas las Conferencias</li>
              <li>Todos los Talleres</li>
            </ul>
            <a href="#" class="boton hollow">Comprar</a>
          </div>
        </li>
        <li>
          <div class="tabla-precio">
            <h3>Todos los Dias</h3>
            <p class="numero">$50</p>
            <ul>
              <li>Bocadillos Gratis</li>
              <li>Todas las Conferencias</li>
              <li>Todos los Talleres</li>
            </ul>
            <a href="#" class="boton">Comprar</a>
          </div>
        </li>
        <li>
          <div class="tabla-precio">
            <h3>Pase por 2 Dias</h3>
            <p class="numero">$45</p>
            <ul>
              <li>Bocadillos Gratis</li>
              <li>Todas las Conferencias</li>
              <li>Todos los Talleres</li>
            </ul>
            <a href="#" class="boton hollow">Comprar</a>
          </div>
        </li>
      </ul>
    </div>
  </section>
  <div id="mapa" class="mapa">

  </div>
  <section class="seccion">
    <h2>Testimoniales</h2>
    <div class="testimoniales contenedor clearfix">
      <div class="testimonial">
        <blockquote>
          <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
      "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
      <footer class="info-testimonial clearfix">
        <img src="img/testimonial.jpg" alt="Imagen Testimonial">
        <cite>Oswaldo Aponte Escobedo <span>Diseñador en @Prisma</span> </cite>
      </footer>
        </blockquote>
      </div><!--Fin del Testimonial-->
      <div class="testimonial">
        <blockquote>
          <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
      "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
      <footer class="info-testimonial clearfix">
        <img src="img/testimonial.jpg" alt="Imagen Testimonial">
        <cite>Oswaldo Aponte Escobedo <span>Diseñador en @Prisma</span> </cite>
      </footer>
        </blockquote>
      </div><!--Fin del Testimonial-->
      <div class="testimonial">
        <blockquote>
          <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
      "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
      <footer class="info-testimonial clearfix">
        <img src="img/testimonial.jpg" alt="Imagen Testimonial">
        <cite>Oswaldo Aponte Escobedo <span>Diseñador en @Prisma</span> </cite>
      </footer>
        </blockquote>
      </div><!--Fin del Testimonial-->
    </div>
  </section>
  <div class="newsletter parallax">
    <div class="contenido contenedor">
      <p>Registrate a la Newsletter</p>
      <h3>GDLWebCamp</h3>
      <a href="#" class="boton transparente">Registro</a>
    </div>
  </div><!--Termina Testimoniales-->
  <section class="seccion">
    <h2>Faltan</h2>
    <div class="cuenta-regresiva contenedor">
      <ul class="clearfix">
        <li><p id="dias" class="numero"></p>Dias</li>
        <li><p id="horas" class="numero"></p>Horas</li>
        <li><p id="minutos" class="numero"></p>Minutos</li>
        <li><p id="segundos" class="numero"></p>Segundos</li>

      </ul>

    </div>

  </section>

  <?php include_once 'includes/templates/footer.php' ?>
