<footer class="site-footer">
  <div class="contenedor clearfix">
    <div class="footer-informacion">
      <h3>Sobre <span>gdlwebcamp</span></h3>
      <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."
  "There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain..."</p>
    </div>
    <div class="ultimos-tweets">
      <h3>Ultimos <span>tweets</span></h3>
      <ul>
        <li>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</li>
        <li>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</li>
        <li>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</li>
      </ul>
    </div>
    <div class="menu">
      <h3>redes <span>Sociales</span></h3>
      <nav class="redes-sociales">
        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-pinterest-p"></i></a>
        <a href="#"><i class="fab fa-youtube"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
      </nav>
    </div>
  </div>
  <p class="copyright">
    Todos los Derechos Reservados GDLWebCamp 2016
  </p>
</footer>

<script src="js/vendor/modernizr-3.6.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/jquery.waypoints.min.js"></script>

<?php
  $archivo = basename($_SERVER['PHP_SELF']);
  $pagina = str_replace(".php", "", $archivo);
  if ($pagina == 'invitados' or $pagina == 'index') {
    echo '<script src="js/jquery.colorbox.js"></script>';
  } elseif ($pagina == 'conferencia') {
    echo '<script src="js/lightbox.js"></script>';
  }

  if ($pagina == 'index') {
    echo '<script src="leaflet/leaflet.js"></script>
    <script src="js/jquery.animateNumber.js"></script>
    <script src="js/jquery.countdown.min.js"></script>';
  }

 ?>

<script src="js/jquery.colorbox.js"></script>
<script src="js/jquery.lettering.js"></script>
<script src="js/main.js"></script>

</body>

</html>
