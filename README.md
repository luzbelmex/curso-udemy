# Curso de Udemy / Web para Conferencia

_Repositorio del curso de udemy de alumno_

## Curso en Udemy

```
https://www.udemy.com/desarrollo-web-completo-con-html5-css3-js-php-y-mysql/
```

## Lenguages que se usan hasta el momento

* HTML5
* CSS3
* JavaScript

## Autores

* **Jose Alatorre** - *Maestro*
* **Samuel Arrieta** - *Alumno*

## Pluging's de JQuery

* Lettering
* Animate Number
* Countdown
